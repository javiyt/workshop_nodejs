var app = require('http').createServer(handler), 
  io = require('socket.io').listen(app), 
  fs = require('fs'),
  Web = require( './lib/web' );

app.listen( 8080 );

function handler( req, res ) 
{
  fs.readFile( __dirname + '/socket.html', function ( err, data ) 
  {
    if ( err ) 
    {
      res.writeHead( 500 );
      return res.end( 'Error loading socket.html' );
    }

    res.writeHead( 200 );
    res.end( data );
  } );
}

io.sockets.on( 'connection', function ( socket ) 
{
  var web = new Web();

  web.mongoGetData( function ( video_list )
  {
    socket.emit( 'videos', video_list );
  } );

  socket.on( 'from_client', function ( data ) 
  {
    console.log( data );
  });
});