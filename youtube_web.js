var http = require( 'http' );
var Web = require( './lib/web' );

http.createServer( function( req, res )
{
	var web = new Web();	
		
	res.writeHead( 200, {'Content-Type': 'text/html'} );

	if ( req.url.match( /video/ ) )
	{
		web.showVideo( req.url, res );
	}
	else
	{
		web.showVideoList( res );
	}
}).listen( 8080 );