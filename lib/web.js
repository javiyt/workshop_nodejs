var mongo = require( 'mongodb' ).MongoClient;

function Web()
{}

Web.prototype.mongoGetData = function( cb, video )
{
	mongo.connect( 'mongodb://127.0.0.1:27017/youtube', function( err, db ) 
	{
		if( err ) throw err;

		var collection = db.collection( 'videos' );

		if ( video )
		{
			collection.findOne( {'_id': video}, function( err, found )
			{
				if ( err ) throw err;

				cb( found );
			} );			
		}
		else
		{
			collection.find( {}, function( err, cursor )
			{
				if ( err ) throw err;

				cursor.toArray( function( err, videos )
				{
					if ( err ) throw err;

					cb( videos );
				} );
			} );
		}
	} );
}

Web.prototype.showVideoList = function( response )
{
	this.mongoGetData( function( res )
	{
		var html = '<ul>';

		for ( var i = res.length - 1; i >= 0; i-- ) 
		{
			html += '<li><a href="/video/' + res[i]['_id'] + '">' + res[i]['title'] + '</a></li>';
		}

		html += '</ul>';
		response.end( html );
	});
}

Web.prototype.showVideo = function( url, response )
{
	var path = url.split( '/' );
	
	this.mongoGetData( function( res )
	{
		var html = '<h1>' + res['title'] + '</h1>';

		html += '<iframe width="560" height="315" src="//www.youtube.com/embed/' + res['_id'] + '" frameborder="0" allowfullscreen></iframe>';
		html += '<p><a href="/">Back</a></p>';

		response.end( html );
	}, path[2] );
}

module.exports = Web;