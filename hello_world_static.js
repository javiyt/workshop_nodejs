var http = require( 'http' );
var server_static = require( 'node-static' );
var file = new server_static.Server();

http.createServer( function( req, res )
{
	file.serve( req, res );
} ).listen( 8080 );