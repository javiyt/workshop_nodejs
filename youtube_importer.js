var http = require( 'https' );
var mongo = require( 'mongodb' ).MongoClient;

function saveData( response )
{
	mongo.connect( 'mongodb://127.0.0.1:27017/youtube', function( err, db ) 
	{
		if( err ) 
		{
			throw err;
		}

		var collection = db.collection( 'videos' ),
			video_info = {},
			total_videos = response.feed.entry.length,
			inserted = 0;

		for ( var i = total_videos - 1; i >= 0; i-- ) 
		{
			video_info = {
				_id: response.feed.entry[i]['media$group']['yt$videoid']['$t'],
				title: response.feed.entry[i].title['$t'],
				thumbnail: response.feed.entry[i]['media$group']['media$thumbnail'][0].url
			};

			collection.insert( video_info, function( err, doc ) 
			{
				if ( err ) throw err;

				console.log( 'Inserted video: ' + doc[0]._id );

				if ( total_videos === ++inserted )
				{
					process.exit( 0 );
				}

			} );
		}
	} );
}

http.get({
	host: 'gdata.youtube.com',
	path: '/feeds/api/videos?alt=json&max-results=10&q=drift',
	scheme: 'http',
	headers: {
		'GData-Version': '2',
		'Access-Control-Allow-Credentials': 'false'
	}
}, function( res )
{
	var response = '';

	res.on( 'data', function( data )
	{
		response += data;
	} );

	res.on( 'end', function()
	{
		saveData( JSON.parse( response ) );
	} );

} ).on( 'error', function( err )
{
	console.log( err );
} );